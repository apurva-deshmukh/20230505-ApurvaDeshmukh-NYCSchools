//
//  NYC_SchoolsApp.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

import SwiftUI

@main
struct NYC_SchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView().environmentObject(AuthViewModel.shared)
        }
    }
}
