//
//  Location.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

import MapKit
import Foundation

struct Location: Identifiable {
    let id = UUID()
    let name: String
    let coordinate: CLLocationCoordinate2D
}
