//
//  School.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

import Foundation

struct School: Codable, Identifiable, Hashable {
    var id = UUID()
    
    let dbn: String
    let name: String
    let website: String
    let totalStudents: String
    let neighborhood: String
    let borough: String?
    let overviewParagraph: String
    let phoneNumber: String
    let schoolEmail: String?
    
    // Could use this to display schools based on user's current location
    // Difficult to do this client side since we are implementing pagination, would likely need to modify API call to take in location as an input (if we were using a custom API)
    let latitude: String?
    let longitude: String?
    
    enum CodingKeys: String, CodingKey {
        case dbn = "dbn"
        case name = "school_name"
        case website = "website"
        case totalStudents = "total_students"
        case neighborhood = "neighborhood"
        case borough = "borough"
        case overviewParagraph = "overview_paragraph"
        case phoneNumber = "phone_number"
        case schoolEmail = "school_email"
        case latitude = "latitude"
        case longitude = "longitude"
    }
}
