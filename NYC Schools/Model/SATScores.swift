//
//  SATScores.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

import Foundation

struct SATScores: Codable {
    var id = UUID()
    
    let readingScore: String
    let mathScore: String
    let writingScore: String
    
    enum CodingKeys: String, CodingKey {
        case readingScore = "sat_critical_reading_avg_score"
        case mathScore = "sat_math_avg_score"
        case writingScore = "sat_writing_avg_score"
    }
}

enum scoreType {
    case reading
    case math
    case writing
}

