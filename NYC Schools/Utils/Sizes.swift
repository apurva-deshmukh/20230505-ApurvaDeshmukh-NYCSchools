//
//  Sizes.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

import Foundation

class Sizes {
    
    static let shared = Sizes()
    
    // MARK: Fonts
    
    let biggestFont: CGFloat = 32
    let largeFont: CGFloat = 18
    let regularFont: CGFloat = 16
    let smallFont: CGFloat = 14
    
    // MARK: Buttons
    
    let largeButtonWidth: CGFloat = 200
    
    // MARK: Opacity
    
    let shadowOpacity = 0.3
    
    // MARK: Corner Radius
    
    let defaultRoundedRectangleCR: CGFloat = 10
    
    // MARK: Borders
    
    let defaultBorderOpacity = 0.5
    let defaultBorderWidth: CGFloat = 0.5
    
    // MARK: Map
    
    let mapHeight: CGFloat = 300
}
