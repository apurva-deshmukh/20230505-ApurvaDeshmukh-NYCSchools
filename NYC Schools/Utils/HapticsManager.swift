//
//  HapticsManager.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

import UIKit

class HapticsManager {
    static func play(style: UIImpactFeedbackGenerator.FeedbackStyle) {
        let impact = UIImpactFeedbackGenerator(style: style)
        impact.impactOccurred()
    }
}
