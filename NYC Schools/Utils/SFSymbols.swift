//
//  SFSymbols.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

enum SFSymbols: String {
    case phone = "phone"
    case email = "envelope"
    case website = "globe"
}
