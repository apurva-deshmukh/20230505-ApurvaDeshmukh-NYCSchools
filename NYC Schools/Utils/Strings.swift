//
//  Strings.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

import Foundation

/*
 This class could be used to localize strings in the future.
 */
class Strings {
    
    static let shared = Strings()
    
    let schools = "Schools"
    let avgSATScores = "Average SAT Scores"
    let scoresNotAvailable = "SAT scores are not available for this school."
    let genericError = "Error fetching schools. Please try again later."
    let contactInfo = "Contact Information"
    let aboutTheSchool = "About the School"
    let students = "students"
    let notApplicable = "N/A"
    let math = "Math"
    let reading = "Reading"
    let writing = "Writing"
    let searchPlaceholder = "Search for a school..."
    
    // MARK: Onboarding
    
    let getStarted = "Get Started"
    let next = "Next"
    
    let onboardingTitle1 = "Explore Secondary Education Options"
    let onboardingSubTitle1 = "A guide to high schools in the NYC area"
    let onboardingTitle2 = "Instant Access to Comprehensive School Information"
    let onboardingSubTitle2 = "View school information such as location, average SAT scores, contact information, and more!"
    
}
