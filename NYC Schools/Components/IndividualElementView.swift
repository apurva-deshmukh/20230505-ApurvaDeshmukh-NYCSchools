//
//  IndividualElementView.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

import SwiftUI

struct IndividualElementView: View {
    let title: String
    let color: Color
    
    var body: some View {
        HStack(spacing: 4) {
            Circle()
                .fill(color)
                .frame(width: 10, height: 10)
            Text(title)
                .font(.system(size: Sizes.shared.smallFont))
        }
        .padding(6)
        .background(RoundedRectangle(cornerRadius: Sizes.shared.defaultRoundedRectangleCR).fill(Color.theme.dark.opacity(0.1))
        )
    }
}

struct IndividualScoreView_Previews: PreviewProvider {
    static var previews: some View {
        IndividualElementView(title: "Math: 480", color: Color.theme.primary)
    }
}
