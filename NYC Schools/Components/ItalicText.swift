//
//  ItalicText.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/5/23.
//

import SwiftUI

struct ItalicText: View {
    let text: String
    var body: some View {
        Text(text)
            .font(.system(size: Sizes.shared.regularFont))
            .italic()
    }
}

struct ItalicText_Previews: PreviewProvider {
    static var previews: some View {
        ItalicText(text: Strings.shared.scoresNotAvailable)
    }
}
