//
//  MainButton.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

import SwiftUI

struct MainButton: View {
    let text: String
    let disabled: Bool
    let color: Color
    let maxWidth: CGFloat
    
    var body: some View {
        HStack {
            Spacer()
            Text(text)
                .font(.system(size: Sizes.shared.regularFont, weight: .bold))
                .foregroundColor(disabled ? .theme.dark.opacity(0.5) : .white)
            Spacer()
        }
        .padding(.vertical, 15)
        .background(disabled ? Color.theme.gray.opacity(0.25) : color)
        .cornerRadius(maxWidth / 4)
        .frame(maxWidth: maxWidth)
    }
}

struct MainButton_Previews: PreviewProvider {
    static var previews: some View {
        MainButton(text: Strings.shared.next,
                   disabled: true,
                   color: .theme.primary,
                   maxWidth: Sizes.shared.largeButtonWidth)
    }
}

