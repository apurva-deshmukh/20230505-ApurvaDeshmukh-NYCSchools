//
//  SingleMessageView.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/5/23.
//

import SwiftUI

struct SingleMessageView: View {
    let text: String
    var body: some View {
        VStack {
            Spacer()
            HStack {
                Spacer()
                Text(text)
                    .foregroundColor(.theme.dark)
                    .font(.system(size: Sizes.shared.largeFont, weight: .regular))
                    .multilineTextAlignment(.center)
                Spacer()
            }
            Spacer()
            Spacer()
        }
        .padding(.horizontal, 20)
    }
}

struct SingleMessageView_Previews: PreviewProvider {
    static var previews: some View {
        SingleMessageView(text: Strings.shared.genericError)
    }
}
