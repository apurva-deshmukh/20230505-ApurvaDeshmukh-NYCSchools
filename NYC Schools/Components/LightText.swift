//
//  LightText.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

import SwiftUI

struct LightText: View {
    let text: String
    var body: some View {
        Text(text)
            .font(.system(size: Sizes.shared.regularFont, weight: .light))
    }
}
