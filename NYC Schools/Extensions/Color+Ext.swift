//
//  Color+Ext.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

import SwiftUI

extension Color {
    static let theme = ColorTheme()
}

struct ColorTheme {
    let light = Color("Light")
    let gray = Color("Gray")
    let dark = Color("Dark")
    
    let primary = Color("Primary")
    let secondary = Color("Secondary")
    let tertiary = Color("Tertiary")
}
