//
//  String+Ext.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

import Foundation

extension String {
    /**
     * Cleans a string by removing the Â (I noticed the API sometimes returned this character in the overview paragraph field) and trims whitespace and newlines
     *
     * - Returns:
     *  - the clean String
     */
    func cleanString() -> String {
        return self.filter({ $0 != "Â" }).trimmingCharacters(in: .whitespacesAndNewlines)
    }
}
