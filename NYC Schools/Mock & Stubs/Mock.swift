//
//  Mock.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

import Foundation

let MOCK_SCHOOL: School = School(dbn: "02M260", name: "Clinton School Writers & Artists, M.S. 260", website: "www.theclintonschool.net", totalStudents: "376", neighborhood: "Chelsea-Union Sq", borough: Optional("MANHATTAN"), overviewParagraph: "Students who are prepared for college must have an education that encourages them to take risks as they produce and perform. Our college preparatory curriculum develops writers and has built a tight-knit community. Our school develops students who can think analytically and write creatively. Our arts programming builds on our 25 years of experience in visual, performing arts and music on a middle school level. We partner with New Audience and the Whitney Museum as cultural partners. We are a International Baccalaureate (IB) candidate school that offers opportunities to take college courses at neighboring universities.", phoneNumber: "212-524-4360", schoolEmail: Optional("admissions@theclintonschool.net"), latitude: Optional("40.73653"), longitude: Optional("-73.9927"))
