//
//  MainTabView.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

import SwiftUI

struct MainTabView: View {
    
    // Create more tabs here inside TabView for other functionalities added in the future (wrap inside TabView)
    
    var body: some View {
        NavigationView {
            SchoolsView()
                .navigationTitle(Strings.shared.schools)
                .accentColor(Color.theme.primary)
        }
        
    }
}

struct MainTabView_Previews: PreviewProvider {
    static var previews: some View {
        MainTabView()
    }
}
