//
//  IndividualContactView.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

import SwiftUI

struct IndividualContactView: View {
    let imageName: String
    let title: String
    let url: String
    
    var body: some View {
        HStack {
            Image(systemName: imageName)
            Link(title, destination: URL(string: url)!)
        }
    }
}
