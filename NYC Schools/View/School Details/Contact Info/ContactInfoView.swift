//
//  ContactInfoView.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

import SwiftUI

struct ContactInfoView: View {
    @ObservedObject var viewModel: ContactInfoViewModel
    
    var body: some View {
        HStack {
            VStack(alignment: .leading, spacing: 6) {
                SubtitleText(text: Strings.shared.contactInfo)
                VStack(alignment: .leading, spacing: 10) {
                    
                    // MARK: Phone Number
                    
                    IndividualContactView(imageName: SFSymbols.phone.rawValue, title: viewModel.phoneNumber, url: viewModel.phoneNumberAsURL())
                    
                    // MARK: Email
                    
                    IndividualContactView(imageName: SFSymbols.email.rawValue, title: viewModel.email, url: viewModel.emailAsURL())
                    
                    // MARK: Website
                    
                    IndividualContactView(imageName: SFSymbols.website.rawValue, title: viewModel.website, url: viewModel.websiteAsURL())
                }
            }
            Spacer()
        }
        .padding()
    }
}

struct ContactInfoView_Previews: PreviewProvider {
    static var previews: some View {
        ContactInfoView(viewModel: ContactInfoViewModel(school: MOCK_SCHOOL))
    }
}
