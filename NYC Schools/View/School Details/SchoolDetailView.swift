//
//  SchoolDetailView.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

import SwiftUI
import MapKit

struct SchoolDetailView: View {
    let viewModel: SchoolViewModel
    
    var body: some View {
        ScrollView {
            VStack(spacing: 0) {
                
                // MARK: MapView
                
                if let coord = viewModel.getCoordinates() {
                    let coordinates = CLLocationCoordinate2D(latitude: coord.0, longitude: coord.1)
                    let locations = [Location(name: viewModel.schoolName, coordinate: coordinates)]
                    MapView(locations: locations, mapRegion:  MKCoordinateRegion(center: coordinates, span: MKCoordinateSpan(latitudeDelta: 0.025, longitudeDelta: 0.025)))
                }
                
                // MARK: Main School Information
                
                SchoolMainStack(name: viewModel.schoolName, location: viewModel.locationString, numStudents: viewModel.numStudentsString)
                
                // MARK: Overview Paragraph
                
                OverviewView(aboutText: viewModel.aboutTheSchoolString)
                
                // MARK: SAT Score Information
                
                SATScoresView(viewModel: SATScoresViewModel(dbn: viewModel.schoolDBN))
                
                // MARK: Contact Information
                
                ContactInfoView(viewModel: ContactInfoViewModel(school: viewModel.school))
            }
        }
    }
}

struct SchoolDetailView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolDetailView(viewModel: SchoolViewModel(school: MOCK_SCHOOL))
    }
}
