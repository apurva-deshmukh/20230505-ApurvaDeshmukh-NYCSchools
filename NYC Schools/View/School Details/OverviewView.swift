//
//  OverviewView.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

import SwiftUI

struct OverviewView: View {
    let aboutText: String
    var body: some View {
        VStack(alignment: .leading, spacing: 6) {
            SubtitleText(text: Strings.shared.aboutTheSchool)
            LightText(text: aboutText)
        }.padding()
    }
}

struct OverviewView_Previews: PreviewProvider {
    static var previews: some View {
        OverviewView(aboutText: MOCK_SCHOOL.overviewParagraph)
    }
}
