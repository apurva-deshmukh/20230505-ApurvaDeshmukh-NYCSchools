//
//  SATScoresView.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

import SwiftUI

struct SATScoresView: View {
    @StateObject var viewModel: SATScoresViewModel
    
    var body: some View {
        VStack(alignment: .leading) {
            SubtitleText(text: Strings.shared.avgSATScores)
            HStack {
                Spacer()
                switch viewModel.state {
                case .loading, .defaultState:
                    ProgressView()
                case .error:
                    ItalicText(text: Strings.shared.scoresNotAvailable)
                case .success:
                    if viewModel.allSATScores.isEmpty {
                        ItalicText(text: Strings.shared.scoresNotAvailable)
                    } else {
                        IndividualElementView(title:
                                                viewModel.getTitleText(for: .math),
                                              color: .theme.primary)
                        IndividualElementView(title:
                                                viewModel.getTitleText(for: .reading),
                                              color: .theme.secondary)
                        IndividualElementView(title:
                                                viewModel.getTitleText(for: .writing),
                                              color: .theme.tertiary)
                    }
                }
                Spacer()
            }
            .padding(.vertical)
            .background(RoundedRectangle(cornerRadius: Sizes.shared.defaultRoundedRectangleCR).fill(Color.theme.dark.opacity(0))
                .overlay(
                    RoundedRectangle(cornerRadius: Sizes.shared.defaultRoundedRectangleCR)
                        .stroke(Color.theme.dark.opacity(Sizes.shared.defaultBorderOpacity), lineWidth: Sizes.shared.defaultBorderWidth)
                )
            )
        }.padding()
    }
}

struct SATScoresView_Previews: PreviewProvider {
    static var previews: some View {
        SATScoresView(viewModel: SATScoresViewModel(dbn: MOCK_SCHOOL.dbn))
    }
}
