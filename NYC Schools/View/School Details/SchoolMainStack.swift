//
//  SchoolMainStack.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

import SwiftUI

struct SchoolMainStack: View {
    let name: String
    let location: String
    let numStudents: String
    var body: some View {
        HStack {
            VStack(alignment: .leading, spacing: 10) {
                TitleText(text: name)
                
                LightText(text: location)
                LightText(text: numStudents)
            }
            Spacer()
        }
        .padding()
    }
}

struct SchoolMainStack_Previews: PreviewProvider {
    static var previews: some View {
        let viewModel = SchoolViewModel(school: MOCK_SCHOOL)
        SchoolMainStack(name: viewModel.schoolName, location: viewModel.locationString, numStudents: viewModel.numStudentsString)
    }
}
