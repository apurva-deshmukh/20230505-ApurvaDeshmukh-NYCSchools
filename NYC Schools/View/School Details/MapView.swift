//
//  MapView.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

import SwiftUI
import MapKit

struct MapView: View {
    var locations: [Location] = []
    @State var mapRegion = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0), span: MKCoordinateSpan(latitudeDelta: 0.025, longitudeDelta: 0.025))
    var body: some View {
        Map(coordinateRegion: $mapRegion, annotationItems: locations) { location in
            MapMarker(coordinate: location.coordinate)
        }
        .clipShape(RoundedRectangle(cornerRadius: Sizes.shared.defaultRoundedRectangleCR))
        .frame(height: Sizes.shared.mapHeight)
        .overlay(
            RoundedRectangle(cornerRadius: Sizes.shared.defaultRoundedRectangleCR)
                .stroke(Color.theme.dark.opacity(Sizes.shared.defaultBorderOpacity), lineWidth: Sizes.shared.defaultBorderWidth)
        )
        .shadow(color: Color.theme.dark.opacity(Sizes.shared.shadowOpacity),
                radius: Sizes.shared.defaultRoundedRectangleCR)
        .padding()
        
        
    }
}

struct MapView_Previews: PreviewProvider {
    static var previews: some View {
        MapView()
    }
}
