//
//  OnboardingPageView.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

import SwiftUI

struct OnboardingPageView: View {
    let imageName: String
    let title: String
    let subtitle: String
    let buttonText: String
    
    let buttonAction: () -> ()
    
    var body: some View {
        VStack(spacing: 20) {

            Spacer()
            
            // Onboarding page could use an image here, right now the page looks kind of bland
            
            VStack(spacing: 40) {
                Text(title)
                    .multilineTextAlignment(.center)
                    .foregroundColor(.theme.dark)
                    .font(.system(size: Sizes.shared.biggestFont, weight: .bold))
                
                Text(subtitle)
                    .multilineTextAlignment(.center)
                    .foregroundColor(.theme.gray)
                    .font(.system(size: Sizes.shared.largeFont, weight: .regular))
            }
            
            Spacer()
            
            Button {
                HapticsManager.play(style: .medium)
                buttonAction()
            } label: {
                MainButton(text: buttonText,
                           disabled: false,
                           color: Color.theme.primary,
                           maxWidth: Sizes.shared.largeButtonWidth)
                    .padding(.bottom, 50)
            }
        }
        .padding()
    }
}


struct OnboardingPageView_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingPageView(imageName: "onboarding2",
                           title: Strings.shared.onboardingTitle2,
                           subtitle: Strings.shared.onboardingSubTitle2,
                           buttonText: Strings.shared.getStarted,
                           buttonAction: {})
    }
}
