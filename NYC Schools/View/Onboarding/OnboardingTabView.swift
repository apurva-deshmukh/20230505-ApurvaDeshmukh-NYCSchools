//
//  OnboardingTabView.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

import SwiftUI

struct OnboardingTabView: View {
    @EnvironmentObject var viewModel: AuthViewModel
    @State private var tabSelection = 0
    
    var body: some View {
        TabView(selection: $tabSelection) {
            OnboardingPageView(imageName: "onboarding1",
                               title: Strings.shared.onboardingTitle1,
                               subtitle: Strings.shared.onboardingSubTitle1,
                               buttonText: Strings.shared.next,
                               buttonAction: nextTapped)
            .tag(0)
            
            OnboardingPageView(imageName: "onboarding2",
                               title: Strings.shared.onboardingTitle2,
                               subtitle: Strings.shared.onboardingSubTitle2,
                               buttonText: Strings.shared.getStarted,
                               buttonAction: getStartedTapped)
            .tag(1)
        }
        .tabViewStyle(.page)
        .indexViewStyle(.page(backgroundDisplayMode: .always))
    }
    
    func nextTapped() {
        tabSelection = 1
    }
    
    func getStartedTapped() {
        viewModel.updateAppState(to: .tutCompleted)
    }
}

struct OnboardingTabView_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingTabView()
    }
}
