//
//  SchoolsView.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

import SwiftUI

struct SchoolsView: View {
    @StateObject var viewModel = SchoolFeedViewModel()
    @State private var searchText = ""
    
    var body: some View {
        VStack(alignment: .leading) {
            
            switch viewModel.state {
            case .loading, .defaultState:
                
                // MARK: Loading View
                
                ProgressView()
            case .error:
                
                // MARK: Error View
                
                // In the future, use custom descriptions from error rather than a generic message
                SingleMessageView(text: Strings.shared.genericError)
            case .success, .paginating:
                
                // MARK: List of Schools
                
                List {
                    // Attempt to filter the schools if user is in "search mode"
                    // Important to note that filter will only return what we already currently have (won't include all the schools unless we have scrolled all the way down due to pagination)
                    let schools = searchText.isEmpty ? viewModel.schools : viewModel.filteredItems(searchText)
                    ForEach(schools) { school in
                        NavigationLink {
                            SchoolDetailView(viewModel: SchoolViewModel(school: school))
                            
                        } label: {
                            SchoolCell(viewModel: SchoolViewModel(school: school))
                                .onAppear {
                                    if viewModel.hasReachEnd(of: school) && !viewModel.isLoadingMore {
                                        Task {
                                            await viewModel.fetchMoreSchools()
                                        }
                                    }
                                }
                        }
                        .buttonStyle(PlainButtonStyle())
                    }
                }
                .listStyle(GroupedListStyle())
            }
        }
        .onAppear {
            Task {
                await viewModel.fetchSchools()
            }
        }
        .searchable(
            text: $searchText,
            placement: .navigationBarDrawer(displayMode: .always),
            prompt: Strings.shared.searchPlaceholder
        )
    }
}

struct SchoolsView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolsView()
    }
}
