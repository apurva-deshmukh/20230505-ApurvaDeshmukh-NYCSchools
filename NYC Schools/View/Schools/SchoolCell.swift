//
//  SchoolCell.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

import SwiftUI

struct SchoolCell: View {
    let viewModel: SchoolViewModel
    var body: some View {
        
        // MARK: School Name, Location, Number of Students
        
        SchoolMainStack(name: viewModel.schoolName, location: viewModel.locationString, numStudents: viewModel.numStudentsString)
            .background(RoundedRectangle(cornerRadius: Sizes.shared.defaultRoundedRectangleCR).fill(Color.theme.light))
            .overlay(
                RoundedRectangle(cornerRadius: Sizes.shared.defaultRoundedRectangleCR)
                    .stroke(Color.theme.dark.opacity(Sizes.shared.defaultBorderOpacity), lineWidth: Sizes.shared.defaultBorderWidth)
            )
            .shadow(color: Color.theme.dark.opacity(Sizes.shared.shadowOpacity),
                    radius: Sizes.shared.defaultRoundedRectangleCR)
        
    }
}

struct SchoolCell_Previews: PreviewProvider {
    static var previews: some View {
        SchoolCell(viewModel: SchoolViewModel(school: MOCK_SCHOOL))
    }
}
