//
//  APIError.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

import Foundation

enum APIError: Error {
    case requestFailed(description: String)
    case invalidData
    case invalidUrl
    case responseUnsuccessful(description: String)
    case jsonConversionFailure(description: String)
    case jsonParsingFailure
    case failedSerialization
    case noInternet
}
