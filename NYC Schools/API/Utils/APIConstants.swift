//
//  APIConstants.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

let ALL_SCHOOLS_URL = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
let SAT_BASE_URL = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"

let LIMIT_PARAM = "$limit"
let OFFSET_PARAM = "$offset"
let ORDER_PARAM = "$order"
let DBN_PARAM = "dbn"
let DEFAULT_ORDER_VAL = ":id"

let PAGE_LIMIT = 10
