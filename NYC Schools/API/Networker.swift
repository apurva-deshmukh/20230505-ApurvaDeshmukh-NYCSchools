//
//  Networker.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

import Foundation

protocol Networker {
    var session: URLSession { get }
    func fetch<T: Codable>(type: T.Type, with request: URLRequest) async throws -> T
}

extension Networker {
    
    /**
     * Generic fetch method that attempts to perform a network request and decode into an object
     *
     * - Parameters:
     *  - type: type of object to decode
     *  - request: URLRequest to perform
     *
     * - Returns:
     *  - the decoded object
     *
     * - Throws:
     *  - any error that occurrs when performing the request or decoding the data
     */
    func fetch<T: Codable>(type: T.Type, with request: URLRequest) async throws -> T {
        let (data, response) = try await session.data(for: request)
        guard let httpResponse = response as? HTTPURLResponse else {
            throw APIError.requestFailed(description: "Invalid response")
        }
        guard httpResponse.statusCode == 200 else {
            throw APIError.responseUnsuccessful(description: "Status code: \(httpResponse.statusCode)")
        }

        do {
            let decoder = JSONDecoder()
            return try decoder.decode(type, from: data)
        } catch {
            throw APIError.jsonConversionFailure(description: error.localizedDescription)
        }
    }
}

