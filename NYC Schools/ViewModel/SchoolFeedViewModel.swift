//
//  SchoolFeedViewModel.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

import Foundation
import Combine

enum SchoolFetchState {
    case loading
    case error
    case success
    case paginating
    case defaultState
}

@MainActor
final class SchoolFeedViewModel: ObservableObject {
    
    // MARK: Properties
    
    private let client = NetworkingClient()
    @Published var state: SchoolFetchState = .defaultState
    @Published var schools: [School] = []
    
    var currentPage = 0
    
    // MARK: Computed Properties
    
    var isLoadingMore: Bool {
        return state == .paginating
    }
    
    // MARK: Public Methods
    
    /**
     * Async function that performs the initial API call to fetch schools
     */
    func fetchSchools() async {
        guard schools.count == 0 else { return }
        state = .loading
        await fetchNextSetOfSchools()
    }
    
    /**
     * Async function that fetches next set of schools using offset pagination
     */
    func fetchMoreSchools() async {
        currentPage += PAGE_LIMIT
        state = .paginating
        await fetchNextSetOfSchools()
    }
    
    /**
     * Function to determine if user has reached the end of the current list of schools
     *
     * - Parameters:
     *  - school: the school whose dbn is used to compare
     *
     * - Returns:
     *  - whether or not the school is the last school in the list
     */
    func hasReachEnd(of school: School) -> Bool {
        return schools.last?.dbn == school.dbn
    }
    
    /**
     * Filters school by the given string based on school name, borough, neighborhood
     *
     * - Parameters:
     *  - query: the string to filter by
     *
     * - Returns:
     *  - a list of filtered schools
     */
    func filteredItems(_ query: String) -> [School] {
        let lowercasedQuery = query.lowercased()
        return schools.filter({ $0.name.lowercased().contains(lowercasedQuery) || $0.borough?.lowercased().contains(lowercasedQuery) ?? false || $0.neighborhood.lowercased().contains(lowercasedQuery) })
    }
    
    // MARK: Private Methods
    
    /**
     * Async function that actually performs the API call to fetch schools
     */
    private func fetchNextSetOfSchools() async {
        var url = URLComponents(string: ALL_SCHOOLS_URL)!
        
        url.queryItems = [
            URLQueryItem(name: LIMIT_PARAM, value: "\(PAGE_LIMIT)"),
            URLQueryItem(name: OFFSET_PARAM, value: "\(currentPage)"),
            URLQueryItem(name: ORDER_PARAM, value: DEFAULT_ORDER_VAL)
        ]
        
        let request = URLRequest(url: url.url!)
        
        do {
            let schools = try await client.fetch(type: [School].self, with: request)
            self.schools += schools
            self.state = .success
        } catch {
            state = .error
        }
    }
}
