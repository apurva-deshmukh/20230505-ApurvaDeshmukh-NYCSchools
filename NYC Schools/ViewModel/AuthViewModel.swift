//
//  AuthViewModel.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

import SwiftUI

class AuthViewModel: ObservableObject {
    
    static let shared = AuthViewModel()
    
    @Storage(key: StorageKeys.authState.rawValue, defaultValue: AuthState.newApp)
    var rawState: AuthState {
        didSet {
            state = rawState
        }
    }
    
    @Published var state: AuthState = .newApp

    init() {
        state = rawState
    }
    
    func updateAppState(to newState: AuthState) {
        state = newState
        rawState = newState
    }
}

enum AuthState: String, Codable {
    case newApp
    case tutCompleted
}
