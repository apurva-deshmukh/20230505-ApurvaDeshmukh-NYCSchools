//
//  SchoolViewModel.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

import Foundation

class SchoolViewModel: ObservableObject {
    
    // MARK: Properties
    
    let school: School
    
    // MARK: Initialization
    
    init(school: School) {
        self.school = school
    }
    
    // MARK: Computed Properties
    
    var schoolName: String {
        return school.name
    }
    
    var locationString: String {
        if let borough = school.borough {
            return "📍 \(school.neighborhood), \(borough.capitalized)"
        } else {
            return "📍 \(school.neighborhood)"
        }
    }
    
    var numStudentsString: String {
        return "📊 \(school.totalStudents) \(Strings.shared.students)"
    }
    
    var schoolDBN: String {
        return school.dbn
    }
    
    var aboutTheSchoolString: String {
        return school.overviewParagraph.cleanString()
    }
    
    // MARK: Public Methods
    
    /**
     * Converts string lat, long to a tuple of Doubles
     *
     * - Returns:
     *  - a tuple of Doubles representing (latitude, longitude)
     */
    func getCoordinates() -> (Double, Double)? {
        guard let latString = school.latitude,
              let longString = school.longitude,
              let latitude = Double(latString),
              let longitude = Double(longString)
        else {
            return nil
        }
        return (latitude, longitude)
    }
}
