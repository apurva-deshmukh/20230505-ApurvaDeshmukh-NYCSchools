//
//  ContactInfoViewModel.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

import Foundation

class ContactInfoViewModel: ObservableObject {
    
    let school: School
    
    // MARK: Initialization
    
    init(school: School) {
        self.school = school
    }
    
    // MARK: Computed Properties
    
    var phoneNumber: String {
        return school.phoneNumber
    }
    
    var email: String {
        return school.schoolEmail ?? Strings.shared.notApplicable
    }
    
    var website: String {
        return school.website
    }
    
    // MARK: Public Methods
    
    /**
     * Function to retrieve the phone number as a url so that
     * the user can call the number by tapping on it
     *
     * - Returns:
     *  - the url version of the phone number as a String
     */
    func phoneNumberAsURL() -> String {
        return "tel:\(school.phoneNumber.replacingOccurrences(of: "-", with: ""))"
    }
    
    /**
     * Function to retrieve the email as a url so that
     * the user can open the Mail app by tapping on it
     *
     * - Returns:
     *  - the url version of the email as a String
     */
    func emailAsURL() -> String {
        return "mailto:\(school.schoolEmail ?? "")"
    }
    
    /**
     * Function to retrieve the website as a url so that
     * the user can open the link in a browser by tapping on it
     *
     * - Returns:
     *  - the url version of the website  as a String
     */
    func websiteAsURL() -> String {
        return "https://\(school.website)"
    }
}
