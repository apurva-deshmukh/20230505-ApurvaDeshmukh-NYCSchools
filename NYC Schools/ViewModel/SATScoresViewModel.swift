//
//  SATScoresViewModel.swift
//  NYC Schools
//
//  Created by Apurva Deshmukh on 5/4/23.
//

import Foundation

enum SATFetchState {
    case loading
    case error
    case success
    case defaultState
}

@MainActor
class SATScoresViewModel: ObservableObject {
    
    private let client = NetworkingClient()
    let dbn: String
    var allSATScores: [SATScores] = []
    @Published var satScores: SATScores?
    @Published var state: SATFetchState = .defaultState
    
    var hasScores: Bool {
        return satScores != nil
    }
    
    // MARK: Initialization
    
    init(dbn: String) {
        self.dbn = dbn
        Task {
            await fetchSATScores()
        }
    }
    
    // MARK: Public Methods
    
    /**
     * Async function that actually performs the API call to fetch SAT scores for a school
     */
    func fetchSATScores() async {
        self.state = .loading
        var url = URLComponents(string: SAT_BASE_URL)!
        
        url.queryItems = [
            URLQueryItem(name: DBN_PARAM, value: "\(dbn)")
        ]
        let request = URLRequest(url: url.url!)
        
        do {
            let satScores = try await client.fetch(type: [SATScores].self, with: request)
            self.allSATScores = satScores
           
            self.state = .success
            self.satScores = satScores.first
        } catch {
            self.state = .error
        }
    }
    
    /**
     * Function to retrieve the text for a score
     *
     * - Parameters:
     *  - type: the type of score (math, reading, writing)
     *
     * - Returns:
     *  - the formatted String of the score (e.g. Math: 480)
     */
    func getTitleText(for type: scoreType) -> String {
        switch type {
        case .reading:
            return "\(Strings.shared.reading): \(satScores?.readingScore ?? Strings.shared.notApplicable)"
        case .math:
            return "\(Strings.shared.math): \(satScores?.mathScore ?? Strings.shared.notApplicable)"
        case .writing:
            return "\(Strings.shared.writing): \(satScores?.writingScore ?? Strings.shared.notApplicable)"
        }
    }
}
